<html>
<form action="degrees-to-radians.php" method="post">
    <label>Enter Degrees:  </label>
    <input type="text" name="degrees" value="<?php if (isset($_POST['degrees'])) echo($_POST['degrees']) ?>">  °
    <br><br>
    <input type="submit" name="convert" value="Convert">
    <input type="submit" name="reset" value="Reset">
    <input type="submit" name="swap" value="Swap">
    <br><br>
</form>
</html>
<?php
if (isset($_POST['convert'])){
    $degrees = $_POST['degrees'] ;
    $result = $degrees * (pi()/180);
    print ("Radians result:	" .$result."  rad") ;
}
if (isset($_POST['reset'])){
    header("Refresh:0; url=degrees-to-radians.php");
}
if (isset($_POST['swap'])){
    header('Location: http://web.app.com:8080/lab03/radians-to-degrees.php');
    die();
}
?>
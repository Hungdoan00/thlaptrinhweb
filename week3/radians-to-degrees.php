<html>
<form action="radians-to-degrees.php" method="post">
    <label>Enter Radians:  </label>
    <input type="text" name="radians" value="<?php if (isset($_POST['radians'])) echo($_POST['radians']) ?>">  rad
    <br><br>
    <input type="submit" name="convert" value="Convert">
    <input type="submit" name="reset" value="Reset">
    <input type="submit" name="swap" value="Swap">
    <br><br>
</form>
</html>
<?php
    if (isset($_POST['convert'])){
        $radians = $_POST['radians'] ;
        $result = $radians * (180/pi());
        print ("Degrees result:	" .$result."  °") ;
    }
    if (isset($_POST['reset'])){
        header("Refresh:0; url=radians-to-degrees.php");
    }
    if (isset($_POST['swap'])){
        header('Location: http://web.app.com:8080/lab03/degrees-to-radians.php');
        die();
    }
?>
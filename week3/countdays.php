<html>
    <form action="countdays.php" method="post">
        <label>Your Name: </label>
        <input type="text" name="name1" value="<?php if (isset($_POST['name1'])) echo($_POST['name1']) ?>">
        <label>Your birthday: </label>
        <input type="text" name="birthdays1" value="<?php if (isset($_POST['birthdays1'])) echo($_POST['birthdays1']) ?>" placeholder="ex:'01-06-2000'">
        <br><br>
        <label>Her Name: </label>
        <input type="text" name="name2" value="<?php if (isset($_POST['name2'])) echo($_POST['name2']) ?>">
        <label>Her Birthday: </label>
        <input type="text" name="birthdays2" value="<?php if (isset($_POST['birthdays2'])) echo($_POST['birthdays2']) ?>" placeholder="ex:'01-06-2000'">
        <br><br>
        <input type="submit" name="submit" value="Submit">
    </form>
</html>
<?php
    if (isset($_POST['submit'])){
        $name1 = $_POST['name1'];
        $name2 = $_POST['name2'];
        $birthdays1 = strtotime($_POST['birthdays1']);
        $birthdays2 = strtotime($_POST['birthdays2']);
        $now = time();
        function dayofweek($birthday){
            $dayofweek = date('w',strtotime($birthday));
            if ($dayofweek == 0)return "Sunday";
            if ($dayofweek == 1)return "Monday";
            if ($dayofweek == 2)return "Tuesday";
            if ($dayofweek == 3)return "Wednesday";
            if ($dayofweek == 4)return "Thursday";
            if ($dayofweek == 5)return "Friday";
            if ($dayofweek == 6)return "Saturday";
        }
        $time1 = date('F d, Y',$birthdays1);
        $time2 = date('F d, Y',$birthdays2);
        print ($name1."&nbsp----- Birthday: ".dayofweek($birthdays1).", ".$time1."<br>");
        print ($name2."&nbsp----- Birthday: ".dayofweek($birthdays2).", ".$time2."<br>");
        $day_diff = abs($birthdays1 - $birthdays2) ;
        $countsday = floor($day_diff/(60*60*24));
        print ("Số ngày giữa các ngày: ".$countsday."<br>");
        $years = floor($day_diff / (365*60*60*24));
        $my_age = floor(abs($now - $birthdays1) / (365*60*60*24));
        $her_age = floor(abs($now - $birthdays2) / (365*60*60*24));
        print ("Số năm: ".$years."<br>");
        print ($name1."-".$my_age."<br>");
        print ($name2."-".$her_age);
    }
?>

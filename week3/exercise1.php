<html>
<form action="index.php" method="post">
    <h4>Enter your name and select date and time for the appointment</h4>
    <label>Your name: </label><input type="text" name="name" value="<?php if (isset($_POST['name'])) echo($_POST['name']) ?>"><br><br>
    <label>Date: </label>
    <select name="day" id="day">
        <?php
        for ($i=1;$i<=31;$i++){
            print ("<option value=".$i." >$i</option>" );
        }
        ?>
    </select>
    <select name="month" id="month">
        <?php
        for ($i=1;$i<=12;$i++){
            print ("<option value=".$i.">$i</option>");
        }
        ?>
    </select>
    <select name="year" id="year">
        <?php
        for ($i=1950;$i<=2050;$i++){
            print ("<option value=".$i.">$i</option>");
        }
        ?>
    </select>
    <br><br>
    <label>Time: </label>
    <select name="hour" id="hour">
        <?php
        for ($i=0;$i<=23;$i++){
            print ("<option value=".$i.">$i</option>");
        }
        ?>
    </select>
    <select name="minute" id="minute">
        <?php
        for ($i=0;$i<=59;$i++){
            print ("<option value=".$i.">$i</option>");
        }
        ?>
    </select>
    <select name="second" id="second">
        <?php
        for ($i=0;$i<=59;$i++){
            print ("<option value=".$i.">$i</option>");
        }
        ?>
    </select>
    <br><br>
    <input type="submit" name="submit" value="Submit">
    <input type="submit" name="reset" value="Reset">
</form>
</html>

<?php
function isLeapYear($year){
    if ($year % 4 == 0 && ($year % 100 != 0 || $year %400 == 0)){
        return true;
    }
    else{
        return false;
    }
}
function dayOfMonth($month,$year){
    $dayOfMonth = 31;
    if ($month == 1||$month == 3||$month == 5||$month == 7||$month == 8||$month == 10||$month == 12){
        $dayOfMonth = 31;
    }else if ($month == 4||$month == 6||$month == 9||$month == 11){
        $dayOfMonth = 30;
    }else if ($month == 2){
        if (isLeapYear($year)){
            $dayOfMonth = 29;
        }else{
            $dayOfMonth = 28;
        }
    }
    return $dayOfMonth;
}
if (isset($_POST['submit'])){
    $hour = $_POST['hour'];
    $minute = $_POST['minute'];
    $second = $_POST['second'];
    $day = $_POST['day'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    $daysOfMonth = dayOfMonth($month,$year);
    $time = strtotime($hour.':'.$minute.':'.$second);
    $timeformat = date('H:i:s',$time);
    print ("Hi ".$_POST['name']."<br>" );
    print ("You have choose to have an appointment on ".$timeformat.",");
    print ($day."/".$month."/".$year."<br>");
    print ("More information <br>");
    print ("In 12 hours, the time and date is ");
    $timeformat2 = date('h:i:s A',$time);
    print ($timeformat2.",");
    print ($day."/".$month."/".$year."<br>");
    print ("This month has ".$daysOfMonth." days!");
}
if (isset($_POST['reset'])){
    header("Refresh:0; url=index.php");
}
?>
<html>
<head><title>Messages Board</title></head>
<?php
    $username = $txt = "";
    $error = array();
    if (!isset($_POST['submit'])){
        print ("Đăng tải một thông điệp mới");
    }else {
        if (empty($_POST["name"])) {
            $error['name'] = 'Bạn chưa nhập tên <br>';
        }
        if (empty($_POST['content'])){
            $error['content'] = 'Bạn chưa nhập nội dung <br>';
        }
        if (!$error){
            $content = $_POST['content'];
            $time = time();
            if (!isset($_POST['unknown'])){
                $username = $_POST['name'];
            }else{
                $username = 'Ẩn danh';
            }
            print $username . " says : " .$content;
            $file_path = "posts/";
            $myfile = fopen($file_path."post_".$time.".txt", "w") or die("Unable to open file!");
            fwrite($myfile, $txt);
            fclose($myfile);
            $filename = $file_path."post_".$time.".txt";
            $fp = fopen($filename, "r");//mở file ở chế độ đọc
            $contents = fread($fp, filesize($filename));//đọc file
            echo "<pre>$contents</pre>";//in nội dung của file ra màn hình
            fclose($fp);//đóng file
            $files = array();
            $uploadedFiles = $_FILES['fileToUpload'];
            foreach ($uploadedFiles as $key => $values) {
                foreach ($values as $index => $value) {
                    $files[$index][$key] = $value;
                }
            }
            foreach ($files as $file) {
                $imageFileType = substr($file['name'],strrpos($file['name'],".")+1);
                $fileimgName = "post_" . $time .".".$imageFileType ;
                move_uploaded_file($file['tmp_name'], $file_path . $fileimgName);
            }
        }
        else{
            echo 'Dữ liệu bị lỗi, không thể lưu trữ';
        }
    }
?>
<form action="messageboard.php" method="post" enctype="multipart/form-data">
    <label>Tên người dùng: </label><br>
    <input type="text" name="name" value="<?php if (isset($_POST['name'])) echo($_POST['name']) ?>"><br>
    <?php echo isset($error['name']) ? $error['name'] : ''; ?>
    <input type="checkbox" name="unknown" <?php if (isset($_POST['unknown'])) echo('checked') ?>>
    <label>Đăng thông điệp vô danh</label><br>
    <label>Nội dung thông điệp: </label><br>
    <textarea type="text" name="content"><?php if (isset($_POST['content'])) echo($_POST['content']) ?></textarea><br>
    <?php echo isset($error['content']) ? $error['content'] : ''; ?>
    <label>Hình ảnh kèm theo (tùy chọn): </label><br>
    <input multiple type="file" name="fileToUpload[]" id="fileToUpload"<br><br><br>
    <input type="submit" value="Đăng thông điệp" name="submit">
</form>
</html>
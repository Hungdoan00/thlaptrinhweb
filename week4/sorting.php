<?php
function user_sort($a, $b) {
    // smarts is all-important, so sort it first
    if($b == 'smarts') {
        return 1;
    }
    else if($a == 'smarts') {
        return -1;
    }

    return ($a == $b) ? 0 : (($a < $b) ? -1 : 1);
}
function cmp2($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a > $b) ? 1 : -1;
}

function cmp($a, $b)
{
    return strcasecmp($a, $b);
}

$values = array('name' => 'Buzz Lightyear',
    'email_address' => 'buzz@starcommand.gal',
    'age' => 32,
    'smarts' => 'some');

$values1 = array('name' => 'Buzz Lightyear',
    'email_address' => 'buzz@starcommand.gal',
    'age' => 32,
    'smarts' => 'some');

if(isset($_POST['submitted'])) {
    if($_POST['sort_type'] == 'sort' ) {
        sort($values);
    }
    elseif($_POST['sort_type'] == 'rsort' ) {
        rsort($values);
//        usort($values, 'user_sort');
    }
    elseif($_POST['sort_type'] == 'usort' ) {
        usort($values, 'user_sort');
    }
    elseif ($_POST['sort_type'] == 'ksort'){
        ksort($values);
    }
    elseif ($_POST['sort_type'] == 'krsort'){
        krsort($values);
    }
    elseif ($_POST['sort_type'] == 'uksort'){
        uksort($values, "cmp");
    }
    elseif ($_POST['sort_type'] == 'asort'){
        asort($values);
    }
    elseif ($_POST['sort_type'] == 'arsort'){
        arsort($values);
    }
    elseif ($_POST['sort_type'] == 'uasort'){
        uasort($values, "cmp2");
    }
}
?>

<form action="sorting.php" method="post">
    <p>
        <input type="radio" name="sort_type" value="sort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='sort'){echo "checked";}}?>/>
        Standard sort<br />
        <input type="radio" name="sort_type" value="rsort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='rsort'){echo "checked";}}?> />   Reverse sort<br />
        <input type="radio" name="sort_type" value="usort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='usort'){echo "checked";}}?> />   User-defined sort<br />
        <input type="radio" name="sort_type" value="ksort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='ksort'){echo "checked";}}?> />   Key sort<br />
        <input type="radio" name="sort_type" value="krsort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='krsort'){echo "checked";}}?> />  Reverse key sort<br />
        <input type="radio" name="sort_type" value="uksort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='uksort'){echo "checked";}}?> />  User-defined key sort<br />
        <input type="radio" name="sort_type" value="asort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='asort'){echo "checked";}}?> />  Value sort<br />
        <input type="radio" name="sort_type" value="arsort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='arsort'){echo "checked";}}?> /> Reverse value sort<br />
        <input type="radio" name="sort_type" value="uasort" <?php if (isset($_POST['submitted'])){ if ($_POST['sort_type']=='uasort'){echo "checked";}}?> /> User-defined value sort<br />
    </p>

    <p >
        <input type="submit" value="Sort" name="submitted" />
    </p>

    <p>
        Values unsorted (before sort):
    </p>

    <ul>
        <?php
        foreach($values1 as $key=>$value) {
            echo "<li><b>$key</b>: $value</li>";
        }
        ?>
    </ul>

    <p>
        <?= isset($_POST['submitted']) ? "Values sorted by : " .$_POST['sort_type'] : ""; ?>
    </p>

    <ul>
        <?php
        if (isset($_POST['submitted'])){
            foreach($values as $key=>$value) {
                echo "<li><b>$key</b>: $value</li>";
            }
        }
        ?>
    </ul>
</form>

